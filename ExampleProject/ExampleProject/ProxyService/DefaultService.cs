﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject.ProxyService
{
    public class DefaultService : IProxyService
    {
        public string ConstructUrl(string url)
        {
            return url;
        }

        public int GetConcurrency()
        {
            return 1;
        }
    }
}
