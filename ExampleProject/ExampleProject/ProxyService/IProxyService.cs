﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject.ProxyService
{
    public interface IProxyService
    {
        string ConstructUrl(string url);

        int GetConcurrency();
    }
}
