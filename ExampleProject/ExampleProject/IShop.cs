﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleProject.ProxyService;
using HtmlAgilityPack;

namespace ExampleProject
{
    public abstract class IShop
    {

        public virtual IEnumerable<Link> GetLinks(string url)
        {
            throw new NotImplementedException();
        }

        public virtual string ConstructUrl(IProxyService service, string url, int pageId)
        {
            throw new NotImplementedException();
        }

        public virtual int GetNumberOfPages(string page)
        {
            throw new NotImplementedException();
        }


        public virtual Watch GetWatch(HtmlDocument document)
        {
            throw new NotImplementedException();
        }

        public virtual List<string> GetEntrySpotUrls()
        {
            throw new NotImplementedException();
        }

    }
}
