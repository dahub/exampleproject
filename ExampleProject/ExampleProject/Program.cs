﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleProject.Impl.ChroNext;
using System.Net;
using HtmlAgilityPack;

namespace ExampleProject
{
    class Program
    {
        //Target it to implement all Shops in the folder Impl
        static void Main(string[] args)
        {
            var log = LogManager.GetCurrentClassLogger();
            var shop = new ChroNext();
            log.Info($"Shop = {shop.GetType().Name}");
            ProcessExample(shop);

        }

        private static void ProcessExample(IShop shop)
        {
            var log = LogManager.GetCurrentClassLogger();
            var shopEntryUrls = shop.GetEntrySpotUrls();
            var detailedUrls = new List<Link>();
            log.Info($"{shop.GetType().Name} : Detailed Urls");
            foreach (var shopEntryUrl in shopEntryUrls)
            {
                var urls = shop.GetLinks(shopEntryUrl).ToList();
                foreach (var url in urls)
                {
                    log.Info($"Url = {url.Url}");
                    log.Info($"Price = {url.Price} / Currency = {url.Currency}");
                }
                detailedUrls.AddRange(urls);
            }
            //Get Watch from detailed url
            //Example on 5 url
            var watches = new List<Watch>();
            log.Info($"{shop.GetType().Name} : Watches");
            for (int i = 0; i < 5; i++)
            {
                var url = detailedUrls[i].Url;
                using (var w = new WebClient())
                {
                    w.Headers.Add(HttpRequestHeader.AcceptLanguage, "en");
                    var page = w.DownloadString(url);
                    var document = new HtmlDocument();
                    document.LoadHtml(page);
                    var watch = new Watch();
                    try
                    {
                        watch = shop.GetWatch(document);
                    }
                    catch (Exception ex)
                    {
                        //log.Error($"{ex}");
                        watch.ErrorMessage = ex.Message;
                    }
                    watches.Add(watch);
                }
            }

            foreach (var watch in watches)
            {
                if (string.IsNullOrEmpty(watch.ErrorMessage))
                {
                    log.Info($"watch {watch.Brand} / {watch.ReferenceNumber}");
                }
                else
                {
                    log.Error($"watch {watch.ErrorMessage}");
                }
            }
        }
    }
}
