﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject
{
    public class Link
    {

        public int Id { get; set;}

        public string Url { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }
    }
}
