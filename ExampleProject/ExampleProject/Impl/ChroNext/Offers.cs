﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject.Impl.ChroNext
{
    public class Offers
    {
        //public string __invalid_name__@type { get; set; }
        public int price { get; set; }
        public string itemCondition { get; set; }
        public string availability { get; set; }
        public string priceCurrency { get; set; }
        public string url { get; set; }
    }
}
