﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject.Impl.ChroNext
{
    public class Item
    {
        //public string __invalid_name__@type { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string sku { get; set; }
        public string mpn { get; set; }
        public string brand { get; set; }
        public string url { get; set; }
        public Offers offers { get; set; }
        public string gtin8 { get; set; }
    }
}
