﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject.Impl.ChroNext
{
    public class ItemListElement
    {
        //public string __invalid_name__@type { get; set; }
        public int position { get; set; }
        public Item item { get; set; }
    }
}
