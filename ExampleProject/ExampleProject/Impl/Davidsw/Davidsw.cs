﻿using ExampleProject.ProxyService;
using HtmlAgilityPack;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject.Impl.Davidsw
{
    //TODO: Implementation
    public class Davidsw : IShop
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override IEnumerable<Link> GetLinks(string url)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method has to return the url to next page in the entrySpotUrl
        /// For example considering the entrySpotUrl https://davidsw.com/watches/rolex
        /// with pageId 2 the method has to return https://davidsw.com/watches/rolex/?page_number=2
        /// </summary>
        /// <param name="service"> DefaultService </param>
        /// <param name="url">entrySpotUrl</param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public override string ConstructUrl(IProxyService service, string url, int pageId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method has to return the max number of pages in the entrySpotUrl
        /// For example considering entrySpotUrl https://davidsw.com/watches/rolex
        /// the method should return 6.
        /// The input is used to know limit the pageId in the method ConstructUrl
        /// </summary>
        /// <param name="page">entrySpotUrl</param>
        /// <returns></returns>
        public virtual int GetNumberOfPages(string page)
        {
            throw new NotImplementedException();
        }

        public override Watch GetWatch(HtmlDocument document)
        {
            throw new NotImplementedException();
        }

        public override List<string> GetEntrySpotUrls()
        {
            return new List<string>()
            {
                "https://davidsw.com/watches/rolex",
                "https://davidsw.com/watches/a-lange-sohne/",
                "https://davidsw.com/watches/audemars-piguet/",
                "https://davidsw.com/watches/blancpain/",
                "https://davidsw.com/watches/breitling/",
                "https://davidsw.com/watches/cartier/",
                "https://davidsw.com/watches/corum/",
                "https://davidsw.com/watches/f-p-journe/",
                "https://davidsw.com/watches/franck-muller/",
                "https://davidsw.com/watches/glashutte/",
                "https://davidsw.com/watches/grand-seiko-seiko/",
                "https://davidsw.com/watches/h-moser-cie/",
                "https://davidsw.com/watches/hermes/",
                "https://davidsw.com/watches/hublot/",
                "https://davidsw.com/watches/iwc/",
                "https://davidsw.com/watches/jaeger-lecoultre/",
                "https://davidsw.com/watches/nomos-glashutte/",
                "https://davidsw.com/watches/omega/",
                "https://davidsw.com/watches/panerai/",
                "https://davidsw.com/watches/patek-philippe/"
            };
        }
    }
}
