﻿using ExampleProject.ProxyService;
using HtmlAgilityPack;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject.Impl.Watchmaxx
{
    //TODO: Implementation
    public class Watchmaxx : IShop
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override IEnumerable<Link> GetLinks(string url)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method has to return the url to next page in the entrySpotUrl
        /// For example considering the entrySpotUrl https://davidsw.com/watches/rolex
        /// with pageId 2 the method has to return https://davidsw.com/watches/rolex/?page_number=2
        /// </summary>
        /// <param name="service"> DefaultService </param>
        /// <param name="url">entrySpotUrl</param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        public override string ConstructUrl(IProxyService service, string url, int pageId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method has to return the max number of pages in the entrySpotUrl
        /// For example considering entrySpotUrl https://davidsw.com/watches/rolex
        /// the method should return 6.
        /// The input is used to know limit the pageId in the method ConstructUrl
        /// </summary>
        /// <param name="page">entrySpotUrl</param>
        /// <returns></returns>
        public virtual int GetNumberOfPages(string page)
        {
            throw new NotImplementedException();
        }

        public override Watch GetWatch(HtmlDocument document)
        {
            throw new NotImplementedException();
        }

        public override List<string> GetEntrySpotUrls()
        {
            return new List<string>()
            {
                "https://www.watchmaxx.com/watches"
            };
        }
    }
}
