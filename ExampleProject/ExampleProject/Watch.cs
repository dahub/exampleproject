﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject
{
    public class Watch
    {
        public Watch()
        {
            Images = new ObservableCollection<Image>();
            OtherAttributes = new ObservableCollection<OtherAttribute>();
            ReferenceNumber = string.Empty;
            Description = string.Empty;
            Name = string.Empty;
            Gender = string.Empty;
            Model = string.Empty;
            Availability = string.Empty;
            Condition = string.Empty;
            Brand = string.Empty;
            ScopeOfDelivery = string.Empty;
            Boxing = string.Empty;
            Papers = string.Empty;
            Location = string.Empty;
            Bezel = string.Empty;
            BezelMaterial = string.Empty;
            Clasp = string.Empty;
            ClaspMaterial = string.Empty;
            Bracelet = string.Empty;
            BraceletMaterial = string.Empty;
            BraceletColor = string.Empty;
            Movement = string.Empty;
            Glass = string.Empty;
            Year = string.Empty;
            CaseMaterial = string.Empty;
            CaseSize = string.Empty;
            ExpectedDelivery = string.Empty;
            DeliveryCost = string.Empty;
            BrandOrigin = string.Empty;
            ClaspFeatures = string.Empty;
            BraceletNotes = string.Empty;
            BraceletName = string.Empty;
            MovementCondition = string.Empty;
            CaseBack = string.Empty;
            CaseShap = string.Empty;
            Age = string.Empty;
            ShopCode = string.Empty;
            DialColor = string.Empty;
            DialType = string.Empty;
            DialMarkers = string.Empty;
            DialNumerals = string.Empty;
            WaterResistance = string.Empty;
            PowerReserve = string.Empty;
            ErrorMessage = string.Empty;
        }

        public int Id { get; set; }

        public string ReferenceNumber { get; set; }

        public string Url { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }
        public string Model { get; set; }

        public string Availability { get; set; }

        public string Condition { get; set; }
        public string Brand { get; set; }

        public string ScopeOfDelivery { get; set; }

        public string Boxing { get; set; }

        public string Papers { get; set; }

        public string Location { get; set; }

        public string Bezel { get; set; }

        public string BezelMaterial { get; set; }

        public string Clasp { get; set; }

        public string ClaspMaterial { get; set; }

        public string Bracelet { get; set; }

        public string BraceletMaterial { get; set; }

        public string BraceletColor { get; set; }

        public string Movement { get; set; }

        public string Glass { get; set; }

        public string Year { get; set; }

        public string CaseMaterial { get; set; }

        public string CaseSize { get; set; }

        public string ExpectedDelivery { get; set; }

        public string DeliveryCost { get; set; }

        public string BrandOrigin { get; set; }
        public string ClaspFeatures { get; set; }

        public string BraceletNotes { get; set; }
        public string BraceletName { get; set; }
        public string MovementCondition { get; set; }
        public string CaseBack { get; set; }
        public string CaseShap { get; set; }
        public string Age { get; set; }
        public string ShopCode { get; set; }
        public string DialColor { get; set; }
        public string DialType { get; set; }
        public string DialMarkers { get; set; }
        public string DialNumerals { get; set; }
        public string WaterResistance { get; set; }
        public string PowerReserve { get; set; }

        public string ErrorMessage { get; set; }
        public DateTime Date { get; set; }

        public ObservableCollection<Image> Images { get; set; }

        public ObservableCollection<OtherAttribute> OtherAttributes { get; set; }

    }
}
