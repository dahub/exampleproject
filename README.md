# ExampleProject #

This is an Example Project where I implemented methods on one Shop.

## ToDo
- [ ] Davidsw.cs
- [ ] Montredo.cs
- [ ] Timelessluxwatches.cs
- [ ] Vestiairecollective.cs
- [ ] Watchfinder.cs
- [ ] Watchmaxx.cs
- [ ] ChroNext.cs (not already implemented methods)

## Remarks
You dont need to implemented any kind of IP routing logic.

The above classes are later integrated in a system which already considert IP routing.